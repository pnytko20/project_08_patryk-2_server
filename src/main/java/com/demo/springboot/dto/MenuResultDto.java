package com.demo.springboot.dto;

import java.util.ArrayList;

public class MenuResultDto {
    public final ArrayList<String> records;


    public MenuResultDto() {
        records = null;
    }

    public MenuResultDto(ArrayList records) {
        this.records = records;
    }

    public ArrayList getRecords() {
        return records;
    }

    public int getRecordsSize(){
        return records.size();
    }

}
