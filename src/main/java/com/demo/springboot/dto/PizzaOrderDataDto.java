package com.demo.springboot.dto;

import java.io.Serializable;

public class PizzaOrderDataDto implements Serializable {
    private String[] indexPizzy = new String[100];
    private String[][] tablica_produktow;
    private int globalnaSuma;


    public PizzaOrderDataDto(String[] index, String[][] tablica_produktow, int globalnaSuma) {
        this.indexPizzy = index;
        this.tablica_produktow = tablica_produktow;
        this.globalnaSuma = globalnaSuma;
        System.out.println("###################################");
        for(int i=0; i<index.length;i++){
            System.out.println(index[i]);
        }
        System.out.println("###################################");
    }

    public PizzaOrderDataDto() {
    }

    public String getNazwaPizzy(int i) {
        String a=(indexPizzy[i]);
        System.out.println("****************************");
        System.out.println(a);
        System.out.println("****************************");
        return tablica_produktow[i][0];
    }

    public String getCenaPizzy(int i) {
        return tablica_produktow[i][1];
    }

    public int getSize() {
        return indexPizzy.length;
    }

    public String getSuma() {
        float sum = 0;
        for(int i = 0; i < getSize(); i++){
            sum+=Float.parseFloat(getCenaPizzy(Integer.parseInt(indexPizzy[i])))/100;
        }

        String a = Float.toString(sum);
        return a;
    }

    public float getGlobalnaSuma(){
        Float globalnaSumaReturn = (float)globalnaSuma;
        return globalnaSumaReturn;
    }

    public float getPromocja(){
        Float getPromocjaReturn = getGlobalnaSuma()-Float.parseFloat(getSuma());
        return getPromocjaReturn;
    }
}
