package com.demo.springboot.service.impl;

import com.demo.springboot.dto.MenuResultDto;
import com.demo.springboot.service.BasicFunctionsService;
import org.springframework.stereotype.Service;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

@Service
public class BasicFunctionsServiceImpl implements BasicFunctionsService {


    @Override
    public MenuResultDto odczytajPlikCSV() {

        String file = "MOCK_DATA.csv";
        String delimiter = ",";
        String line;
        ArrayList records = new ArrayList();
        try (Scanner s = new Scanner(new File(file))) {
            while(s.hasNext()){
                line = s.next();
                    List values = Arrays.asList(line.split(delimiter));
                    records.add(values);
            }
            records.forEach(l -> System.out.println(l));
        } catch (Exception e){
            System.out.println(e);
        }

        return new MenuResultDto((ArrayList) records);
    }

    public String[][] odczytajPlikCSVtoArray(int listsize, int col) {

        String file = "MOCK_DATA.csv";
        String delimiter = ",";
        String line;
        ArrayList records = new ArrayList();
        try (Scanner s = new Scanner(new File(file))) {
            while(s.hasNext()){
                line = s.next();
                List values = Arrays.asList(line.split(delimiter));
                records.add(values);
            }
            records.forEach(l -> System.out.println(l));
        } catch (Exception e){
            System.out.println(e);
        }

        String test=records.toString();
        String[] zupa = test.split(",");



        String[][] tablica = new String[listsize][col];

        for (int i = 0; i < listsize; i++) {
            for (int j = 0; j < 5; j++) {
                tablica[i][j] = zupa[5*i+j].replaceAll("\\[", "")
                        .replaceAll("\"","")
                        .replaceAll("\\]","")
                        .replaceAll(" ","");
            }
        }

        return tablica;
    }

}
