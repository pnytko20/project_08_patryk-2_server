package com.demo.springboot.service.impl;

import com.demo.springboot.dto.PizzaOrderDataDto;
import com.demo.springboot.service.WszelkieDzialania;
import org.springframework.stereotype.Service;

@Service
public class WszelkieDzialaniaImpl implements WszelkieDzialania {
    public static String[][] tablica_produktow;
    public String nazwaOstatniej;
    public String nazwaprzedostatniej;
    @Override
    public int sumaKoszyka(String[] a, String[][] tablica_produktow, boolean c){
    int suma=0;

    for( String x : a){
        suma+=Integer.parseInt(tablica_produktow[Integer.parseInt(x)][1]);
        System.out.println(suma);
    }

    if(c && suma>=10000){
        suma*=0.8;
    }

    return suma/100;
    }

    @Override
    public int cenaPojedynczegoProduktu(int index) {
        int pojedynczyProdukt = Integer.parseInt(tablica_produktow[index][1]);
        return 0;
    }

    @Override
    public int dodawaniePromocji(String[] a){
        int cena=0;
        for(int i=a.length; i>(a.length-2); i--){
            cena+=(cenaPojedynczegoProduktu(i)/2);
            System.out.println(cena);
        }
        nazwaOstatniej = tablica_produktow[Integer.parseInt(a[a.length])][0];
        nazwaprzedostatniej=tablica_produktow[Integer.parseInt(a[a.length-1])][0];
        return cena;
    }

    @Override
    public String getNazwaOstatniej(){
        return nazwaOstatniej;
    }

    @Override
    public String getNazwaPrzedostatniej() {
        return nazwaprzedostatniej;
    }


}
