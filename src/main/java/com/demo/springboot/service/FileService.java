package com.demo.springboot.service;

import com.demo.springboot.dto.PizzaOrderDataDto;
import com.demo.springboot.dto.FileData;

import java.util.List;

public interface FileService {

    FileData createFile(PizzaOrderDataDto pizzaOrderDataDto, String path);

    List<FileData> findAll(String path);
}
