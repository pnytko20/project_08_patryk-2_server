package com.demo.springboot.service;

import com.demo.springboot.dto.MenuResultDto;
import com.demo.springboot.service.impl.WszelkieDzialaniaImpl;

public interface WszelkieDzialania {
    int sumaKoszyka(String[] a, String[][] b, boolean c);

    int cenaPojedynczegoProduktu(int index);

    int dodawaniePromocji(String[] a);

    String getNazwaOstatniej();

    String getNazwaPrzedostatniej();
}


