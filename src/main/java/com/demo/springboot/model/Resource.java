package com.demo.springboot.model;

import com.demo.springboot.dto.FileData;

import java.util.List;

public interface Resource {
    String fileName = "files.csv";

    void saveOne(FileData fileData, String path);

    List<FileData> findAll(String path);
}
