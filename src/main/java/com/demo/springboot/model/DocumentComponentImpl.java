package com.demo.springboot.model;

import com.demo.springboot.dto.PizzaOrderDataDto;
import com.demo.springboot.service.WszelkieDzialania;
import com.demo.springboot.service.impl.WszelkieDzialaniaImpl;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.itextpdf.text.Document;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

@Component
public class DocumentComponentImpl implements DocumentComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentComponentImpl.class);

    @Override
    public void createDocument(PizzaOrderDataDto pizzaOrderDataDto, String fileDestination) {
        try {
            final String FONT = "static/arial.ttf";
            Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, true);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileDestination));
            document.open();
            Paragraph preface = new Paragraph();
            preface.add(new Paragraph("PARAGON", font));
            PdfPTable table = new PdfPTable(2);
            table.setWidths(new int[]{2, 1});
            table.addCell(createCell("Nazwa pizzy", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Cena pizzy", 2, Element.ALIGN_RIGHT));

            for(int i = 1; i <= pizzaOrderDataDto.getSize(); i++) {
                table.addCell(createCell(pizzaOrderDataDto.getNazwaPizzy(i), 1, Element.ALIGN_LEFT));
                table.addCell(createCell(Float.parseFloat(pizzaOrderDataDto.getCenaPizzy(i))/100 + " zl", 1, Element.ALIGN_RIGHT));
            }


                table.addCell(createCell("Suma: ", 2, Element.ALIGN_CENTER));
                table.addCell(createCell(pizzaOrderDataDto.getGlobalnaSuma() + " zl", 2, Element.ALIGN_RIGHT));
                table.addCell(createCell("Promocja: ", 2, Element.ALIGN_CENTER));
                table.addCell(createCell(pizzaOrderDataDto.getPromocja() + " zl", 2, Element.ALIGN_RIGHT));
            document.add(table);

            setBackgroundAsGradient(document, writer);

            document.close();

        } catch (DocumentException | FileNotFoundException e) {
            LOGGER.error("i can't create document or file not exists");
        }
    }

    private PdfPCell createCell(String content, float borderWidth, int alignment) {
        final String FONT = "static/arial.ttf";

        Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, true);

        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setBorderWidth(borderWidth);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingTop(3);
        cell.setPaddingBottom(6);
        cell.setPaddingLeft(3);
        cell.setPaddingRight(3);
        return cell;
    }

    private void setBackgroundAsGradient(Document document, PdfWriter writer) {
        Rectangle pageSize = document.getPageSize();
        PdfShading axial = PdfShading.simpleAxial(writer,
                pageSize.getLeft(pageSize.getWidth()/10), pageSize.getBottom(),
                pageSize.getRight(pageSize.getWidth()/10), pageSize.getBottom(),
                new BaseColor(255, 255, 255),
                new BaseColor(255, 255, 255), true, true);
        PdfContentByte canvas = writer.getDirectContentUnder();
        canvas.paintShading(axial);
    }
}
