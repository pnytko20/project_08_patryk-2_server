package com.demo.springboot.model;

import com.demo.springboot.dto.PizzaOrderDataDto;

public interface DocumentComponent {
    void createDocument(PizzaOrderDataDto pizzaOrderDataDto, String fileDestination);
}
