package com.demo.springboot.rest;

import com.demo.springboot.dto.FileData;
import com.demo.springboot.dto.KoszykDto;
import com.demo.springboot.dto.MenuResultDto;
import com.demo.springboot.dto.PizzaOrderDataDto;
import com.demo.springboot.service.FileService;
import com.demo.springboot.service.BasicFunctionsService;
import com.demo.springboot.service.WszelkieDzialania;
import com.demo.springboot.service.impl.WszelkieDzialaniaImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
@RequestMapping("/api")
public class FunctionApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FunctionApiController.class);

    private final BasicFunctionsService basicFunctionsService;
    private final WszelkieDzialania wszelkieDzialania;
    public FunctionApiController(BasicFunctionsService basicFunctionsService, WszelkieDzialania wszelkieDzialania) {
        this.basicFunctionsService = basicFunctionsService;
        this.wszelkieDzialania = wszelkieDzialania;
    }

    KoszykDto koszykDto=new KoszykDto();

    private static final String PATH = "paragony\\";

    MenuResultDto menuResultDtoServer;
    @Autowired
    private FileService fileService;

    //ZMIENNE
    String[][] tablica_produktow;
    int globalnaSuma;

    @CrossOrigin
    @GetMapping(value = "/pizzeria/wyswietlPizze")
    public ResponseEntity<MenuResultDto> wyswietlPizze() {
        LOGGER.info("--- KLIENT PROSI O LISTE PIZZ = " );
        LOGGER.info("--- PIERWSZY MAPPING " );

        final MenuResultDto menuResultDto = basicFunctionsService.odczytajPlikCSV();
        menuResultDtoServer = menuResultDto;

        tablica_produktow= basicFunctionsService.odczytajPlikCSVtoArray(menuResultDto.records.size(),5);
        WszelkieDzialaniaImpl.tablica_produktow=basicFunctionsService.odczytajPlikCSVtoArray(menuResultDto.records.size(),5);

        return ResponseEntity.ok(menuResultDto);

    }

    @CrossOrigin
    @GetMapping(value = "/pizzeria/wyslijKoszyk")
    public ResponseEntity wyslijKoszyk(@RequestParam("a") String[] a, @RequestParam("b") boolean c) {
        LOGGER.info("--- AKTUALNY STAN KOSZYKA " );
        int suma= wszelkieDzialania.sumaKoszyka(a,tablica_produktow, c);

        koszykDto.dodajCene(suma);

        globalnaSuma=suma;
        LOGGER.info("--- PROMOCJA: {}",c);
        LOGGER.info("--- SUMA: {}",suma);

        koszykDto.dodajPizze(a, tablica_produktow);
        //koszykDto.dodajCene(a,tablica_produktow);

        for( String x : a){
            LOGGER.info("--- Wybrany produkt: {}",tablica_produktow[Integer.parseInt(x)][0]);
        }
        return ResponseEntity.ok(koszykDto);
    }

    @CrossOrigin
    @GetMapping(value = "/pizzeria/wyslijPromocjaPiedziesiat")
    public ResponseEntity wyslijPromocjaPiedziesiat(@RequestParam("a") String[] a) {
        LOGGER.info("--- PRZESŁANO 50/50 " );
        globalnaSuma+= wszelkieDzialania.dodawaniePromocji(a);

        //WPISAĆ GET NAZWA OSTATNI I GET NAZWA PRZEDOSTATNI DO PDF

        //KoszykDto koszykDto=new KoszykDto(suma);

        for( String x : a){
            LOGGER.info("--- Wybrany produkt: {}",tablica_produktow[Integer.parseInt(x)][0]);
        }
        return ResponseEntity.ok(tablica_produktow);
    }


    @CrossOrigin
    @GetMapping(value = "/pizzeria/zamowionePizze")
    public ResponseEntity<?> zamowionePizze(@RequestParam("a") String[] a){

        PizzaOrderDataDto pizzaOrderDataDto = new PizzaOrderDataDto(a,tablica_produktow,globalnaSuma);
        final MenuResultDto menuResultDto = basicFunctionsService.odczytajPlikCSV();


        LOGGER.info("--- GORDON, DOSTALISMY NAZWY PIZZ: {}", Arrays.toString(a));
        LOGGER.info("--- DRUGI MAPPING {}", menuResultDto.records.size() );
        //TEST VV
        for( String x : a){
            LOGGER.info("--- Wybrany produkt: {}",tablica_produktow[Integer.parseInt(x)][0]);
        }


        globalnaSuma=0;

        FileData fileData = fileService.createFile(pizzaOrderDataDto, PATH);

        com.demo.springboot.dto.ErrorDto errorMessage = new com.demo.springboot.dto.ErrorDto(com.demo.springboot.dto.ErrorMessage.ERROR_PATH.getErrorMessage());
        HttpStatus errorCode = com.demo.springboot.dto.ErrorMessage.ERROR_PATH.getErrorCode();
        return fileData != null ?
                new ResponseEntity<>(fileData, HttpStatus.CREATED) :
                new ResponseEntity<>(errorMessage, errorCode);

    }
}